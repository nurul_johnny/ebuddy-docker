FROM ubuntu:18.04

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update \
    && apt-get install -y locales \
    && echo "UTC+6" > /etc/timezone \
    && locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update \
    && apt-get install -y curl zip unzip git supervisor cron software-properties-common \
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y php7.3-fpm php7.3-cli php7.3-gd php7.3-mysql php7.3-pgsql php7.3-xdebug php7.3-imap \
    && apt-get install -y php7.3-memcached php7.3-mbstring php7.3-xml php7.3-curl php7.3-zip php7.3-sqlite \
    && apt-get install -y php7.3-bcmath php7.3-pdo php7.3-bz2 php7.3-redis php7.3-soap \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && mkdir /run/php \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN sed -i -e "s/;\?daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.3/fpm/php-fpm.conf
RUN sed -i s'/listen = \/run\/php\/php7.3-fpm.sock/listen = 0.0.0.0:9000/' /etc/php/7.3/fpm/pool.d/www.conf

ADD ./php.ini /etc/php/7.3/fpm/php.ini
ADD ./php-fpm.conf /etc/php/7.3/fpm/pool.d/www.conf

ADD ./supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD ./cron-conf /etc/cron.d/cron-conf

RUN crontab /etc/cron.d/cron-conf

EXPOSE 9000

CMD ["supervisord"]
